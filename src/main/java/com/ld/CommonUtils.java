package com.ld;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialogLayout;
import com.ld.netty.common.Config;
import com.ld.view.Login;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.apache.commons.io.IOUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

@Slf4j
public class CommonUtils {

    public static File skynetFile;

    private static String rootPath;

    static RandomAccessFile randomAccessFile;
    private static FileChannel fileChannel;
    static FileLock fileLock;

    static ResourceBundle globalization = ResourceBundle.getBundle("locales.content");

    public static String getLocaleContent(String key){
        return globalization.getString(key);
    }

    public static JFXAlert alert(Window window,Node heading, Node body, Node... actions){
        JFXAlert alert = new JFXAlert(window);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setOverlayClose(false);
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setPrefWidth(240);
        layout.setHeading(heading);
        layout.setBody(body);
        layout.setActions(actions);
        alert.setContent(layout);
        return alert;
    }


    public static void alertMsg(Window window,String msg){
        JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
        javafx.scene.control.Label alertTitle = new javafx.scene.control.Label(CommonUtils.getLocaleContent("alertTitle"));
        javafx.scene.control.Label alertMsgNetworkError = new javafx.scene.control.Label(msg);
        JFXAlert alert = alert(window, alertTitle, alertMsgNetworkError, close);
        close.setOnAction(event1 -> alert.hideWithAnimation());
        alert.show();;
    }

    public static boolean isAutoLogin(){
        if (skynetFile != null){
            try {
                byte[] bytes = Files.readAllBytes(skynetFile.toPath());
                if (bytes.length > 0){
                    String json = new String(bytes, Charset.forName("utf-8"));
                    System.out.println(json);
                    JSONArray objects = JSON.parseArray(json);
                    for (int i = 0; i < objects.size(); i++) {
                        JSONObject object = objects.getJSONObject(i);
                        if (object.getBoolean("autoLogin")){
                            String key = object.getString("key");
                            String host = object.getString("public_ip");
                            boolean sysLogin = getSysLogin(key);
                            if (!sysLogin){
                                Config.getInstance().init(host,4900,key);
                                System.setProperty(Login.automaticLogin,"true");
                                System.setProperty(Login.automaticSavePassword,"true");
                                return true;
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static boolean isSavePassword(){
        if (skynetFile != null){
            try {
                byte[] bytes = Files.readAllBytes(skynetFile.toPath());
                if (bytes.length > 0){
                    JSONArray objects = JSON.parseArray(new String(bytes));
                    if (objects.size() > 0){
                        JSONObject object = objects.getJSONObject(objects.size()-1);
                        String key = object.getString("key");
                        String host = object.getString("public_ip");
                        Config.getInstance().init(host,4900,key);
                        System.setProperty(Login.automaticLogin,"false");
                        System.setProperty(Login.automaticSavePassword,"true");
                        return true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    public static void setSysLogin(String key){
        Path path = Paths.get(rootPath, key + "-login");
        try {
            Files.write(path,"1".getBytes(),StandardOpenOption.CREATE);
            randomAccessFile = new RandomAccessFile(path.toFile(), "rw");
            fileChannel = randomAccessFile.getChannel();
            fileLock = fileChannel.tryLock();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean getSysLogin(String key){
        Path path = Paths.get(rootPath, key + "-login");
        if (path.toFile().exists()){
            RandomAccessFile raf = null;
            FileChannel channel = null;
            FileLock fileLock = null;
            try {
                raf = new RandomAccessFile(path.toFile(), "rw");
                channel = raf.getChannel();
                fileLock = channel.tryLock();
                if (fileLock != null && fileLock.isValid()){
                    return false;
                }
                return true;
            } catch (Exception e) {
                return true;
            }finally {
                if (fileLock != null){
                    try {
                        fileLock.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (channel != null){
                    try {
                        channel.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (raf != null){
                    try {
                        raf.close();
                        System.out.println("关闭raf");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }

    public static void delSysLogin(String key){
        Path path = Paths.get(rootPath, key + "-login");
        if (path.toFile().exists()){
            try {
                fileLock.close();
                fileChannel.close();
                randomAccessFile.close();
            }catch (IOException e){
                e.printStackTrace();
            }
            path.toFile().delete();
        }
    }


    public static void initSkynet(){
        String property = System.getProperty("user.home");
        Path path = Paths.get(property, "skynet");
        Path skynet = Paths.get(path.toFile().getAbsolutePath(), "skynet.json");
        if (!path.toFile().exists()){
            path.toFile().mkdirs();
        }
        if (path.toFile().exists()){
            rootPath = path.toFile().getAbsolutePath();
        }
        if (!skynet.toFile().exists()){
            try {
                skynet.toFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (skynet.toFile().exists()){
            skynetFile = skynet.toFile();
        }
    }

    public static void saveUserInfo(JSONObject data){
        if (skynetFile != null){
            try {
                JSONArray objects = new JSONArray();
                byte[] bytes = Files.readAllBytes(skynetFile.toPath());
                if (bytes.length > 0) {
                    objects = JSON.parseArray(new String(bytes));
                }
                if (objects.size() > 0){
                    for (int i = 0; i < objects.size(); i++) {
                        JSONObject jsonObject = objects.getJSONObject(i);
                        if (jsonObject.getString("key").equals(data.getString("key"))){
                            objects.remove(i);
                            break;
                        }
                    }
                }
                objects.add(data);
                log.debug("CommonUtils->saveUserInfo:{}",data.toJSONString());
                Files.delete(skynetFile.toPath());
                Files.write(skynetFile.toPath(),objects.toJSONString().getBytes(), StandardOpenOption.CREATE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Getter
    static javafx.scene.image.Image skynetTitle;
    static Image tray_off;
    static Image tray_on;
    static TrayIcon trayIcon;



    public static void setTrayIcon(int status){
        if (status == 0){
            trayIcon.setImage(tray_off);
        }else {
            trayIcon.setImage(tray_on);
        }
    }

    public static void initIons(){
        tray_off = getAWTImage("/icons/tray_off.png");
        tray_on = getAWTImage("/icons/tray_on.png");
        skynetTitle = getFXImage("/icons/skynet_title.png");
    }

    public static Image getAWTImage(String url){
        Image image = null;
        InputStream resourceAsStream = Main.class.getResourceAsStream(url);
        if (resourceAsStream != null){
            try {
                image = new ImageIcon(IOUtils.toByteArray(resourceAsStream)).getImage();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (resourceAsStream != null) {
                    IOUtils.closeQuietly(resourceAsStream);
                }
            }
        }
        return image;
    }

    public static javafx.scene.image.Image getFXImage(String url){
        javafx.scene.image.Image image = null;
        InputStream resourceAsStream = Main.class.getResourceAsStream(url);
        if (resourceAsStream != null){
            try {
                image = new javafx.scene.image.Image(resourceAsStream);
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                if (resourceAsStream != null) {
                    IOUtils.closeQuietly(resourceAsStream);
                }
            }
        }
        return image;
    }

    public static void systemTray(){
        if (SystemTray.isSupported()){
            trayIcon = new TrayIcon(tray_off, "\u516c\u7f51\u0049\u0050\u002d"+Config.getInstance().getHost());
            PopupMenu jPopupMenu = getJPopupMenu();
            trayIcon.setPopupMenu(jPopupMenu);
            trayIcon.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (stage != null && !stage.isShowing()){
                        Platform.runLater(()->{
                            stage.show();
                        });
                    }
                }
            });
            try {
                SystemTray.getSystemTray().add(trayIcon);
            } catch (AWTException e) {
                e.printStackTrace();
            }
        }
    }

    public static void delSystemTray(){
        if (SystemTray.isSupported()){
            SystemTray.getSystemTray().remove(trayIcon);
        }
    }

    public static PopupMenu getJPopupMenu(){
        PopupMenu popupMenu = new PopupMenu(); //创建弹出菜单对象
        MenuItem close = new MenuItem("\u9000\u51fa");    //创建弹出菜单中的退出项
        MenuItem console = new MenuItem("\u663e\u793a\u63a7\u5236\u53f0"); //创建弹出菜单中的显示主窗体项.
        close.addActionListener(e -> {
            System.exit(0);
        });
        console.addActionListener(e -> {
            if (stage != null){
                Platform.runLater(()->{
                    stage.show();
                });
            }
        });
        popupMenu.add(console);
        popupMenu.add(close);
        return popupMenu;
    }
    public static Stage stage;
    public static void setStage(Stage stage){
        CommonUtils.stage = stage;
    }
    public static void main(String[] args) throws IOException {
//        System.setProperty("jkljdfwejfewfijewiofj","1");
        System.out.println("网络是否可用:"+isConnect());
//        try {
//            Thread.sleep(3000000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }


    public static boolean isConnect(){
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10,TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder().url("https://www.baidu.com/").build();
        try {
            String string = okHttpClient.newCall(request).execute().body().string();
            if (string != null && string.length() > 0){
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
