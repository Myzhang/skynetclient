package com.ld;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class OpenApi {

    static OpenApi openApi = new OpenApi();

    OkHttpClient httpClient;
    public OpenApi(){
        httpClient = new OkHttpClient()
                .newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();
    }

    public static OpenApi getInstance(){
        return openApi;
    }

    public String get(String url){
        Request request = new Request.Builder().url(url).build();
        try {
            String string = httpClient.newCall(request).execute().body().string();
            return string;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void get(String url,Callback callback){
        Request request = new Request.Builder().url(url).build();
        httpClient.newCall(request).enqueue(callback);
    }
}
