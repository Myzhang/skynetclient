package com.ld.view;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfoenix.controls.*;
import com.ld.CommonUtils;
import com.ld.OpenApi;
import com.ld.netty.common.Config;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class Login {

    private AnchorPane anchorPane;
    private JFXTextField userNameField;
    private JFXPasswordField passwordField;
    private JFXButton loginButton;
    private JFXCheckBox savePwd;
    private JFXCheckBox autoLogin;
    private JFXRippler title;
    private Stage stage;

    public static String automaticSavePassword = "skynet_automatic_save_password";
    public static String automaticLogin = "skynet_automatic_login";

    public Login(Stage stage){
        this.stage = stage;
        anchorPane = new AnchorPane();
        userNameField = new JFXTextField();
        passwordField = new JFXPasswordField();
        loginButton = new JFXButton(CommonUtils.getLocaleContent("login"));
        savePwd = new JFXCheckBox(CommonUtils.getLocaleContent("savePassword"));
        autoLogin = new JFXCheckBox(CommonUtils.getLocaleContent("autoLogin"));
        Label label = new Label(CommonUtils.getLocaleContent("appTitle"));
        label.setStyle("-fx-font-size: 32px");
        title = new JFXRippler(label);
        userNameField.setPromptText(CommonUtils.getLocaleContent("tipsUserName"));
        passwordField.setPromptText(CommonUtils.getLocaleContent("tipsPassword"));
        loginButton.setStyle("-jfx-button-type:RAISED");
        title.setStyle("-jfx-rippler-recenter:true;-jfx-rippler-fill:red;-jfx-rippler-radius:1;-jfx-mask-type:RECT");

        if (CommonUtils.isAutoLogin() || CommonUtils.isSavePassword()){
            userNameField.setText(Config.getInstance().getHost());
            passwordField.setText(Config.getInstance().getKey());
        }
        String autoSavePwd = System.getProperty(automaticSavePassword);
        savePwd.setSelected("true".equals(autoSavePwd == null ? "true" : autoSavePwd));

        String autoLoginEnv = System.getProperty(automaticLogin);
        autoLogin.setSelected("true".equals(autoLoginEnv == null ? "true" : autoLoginEnv));

        ObservableList<Node> anchorPaneChildren = anchorPane.getChildren();
        anchorPaneChildren.addAll(userNameField,passwordField,loginButton,savePwd,autoLogin,title);

        AnchorPane.setTopAnchor(title,30.0);
        AnchorPane.setLeftAnchor(title,30.0);
        AnchorPane.setRightAnchor(title,30.0);

        AnchorPane.setTopAnchor(userNameField,170.0);
        AnchorPane.setLeftAnchor(userNameField,30.0);
        AnchorPane.setRightAnchor(userNameField,30.0);

        AnchorPane.setTopAnchor(passwordField,200.0);
        AnchorPane.setLeftAnchor(passwordField,30.0);
        AnchorPane.setRightAnchor(passwordField,30.0);

        AnchorPane.setTopAnchor(loginButton,230.0);
        AnchorPane.setLeftAnchor(loginButton,30.0);
        AnchorPane.setRightAnchor(loginButton,30.0);

        AnchorPane.setBottomAnchor(savePwd,10.0);
        AnchorPane.setLeftAnchor(savePwd,30.0);

        AnchorPane.setBottomAnchor(autoLogin,10.0);
        AnchorPane.setRightAnchor(autoLogin,30.0);
        loginButton.setOnAction(event -> {
            String userName = userNameField.getText();
            String passWord = passwordField.getText();
            if (userName.length() == 0 || passWord.length() == 0){
                JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
                JFXAlert alert = CommonUtils.alert(stage.getScene().getWindow(),
                        new Label(CommonUtils.getLocaleContent("alertTitle")),
                        new Label(CommonUtils.getLocaleContent("alertMsgUserNameOrPasswordNull")),
                        close);
                close.setOnAction(event12 -> alert.hideWithAnimation());
                alert.show();
                return;
            }
            loginButton.setDisable(true);
            loginButton.setText(CommonUtils.getLocaleContent("alertLoading"));
            new Thread(()->{
                //检查网络是否通畅 最多10分钟
                int k = 10 * 60;
                boolean network = false;
                for (int i = 0; i < k; i++) {
                    if (CommonUtils.isConnect()){
                        network = true;
                        break;
                    }
                    System.out.println("network false");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (network){
                    String s = OpenApi.getInstance().get("http://api.tbytm.com/open/v2/login?key=" + passWord);
                    if (s != null){
                        try {
                            JSONObject object = JSON.parseObject(s);
                            if (object.getInteger("code") == 0){
                                JSONObject data = object.getJSONObject("data");
                                if (data.getString("public_ip").equals(userName)){
                                    if (data.getInteger("status") == 0){
                                        Platform.runLater(()->{
                                            JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
                                            JFXAlert alert = CommonUtils.alert(stage.getScene().getWindow(),
                                                    new Label(CommonUtils.getLocaleContent("alertTitle")),
                                                    new Label(CommonUtils.getLocaleContent("alertMsgArrears")),
                                                    close);
                                            close.setOnAction(event1 -> alert.hideWithAnimation());
                                            alert.show();
                                        });
                                    }else {
                                        System.out.println(data.toJSONString());
                                        if (savePwd.isSelected()){
                                            data.put("autoLogin",autoLogin.isSelected());
                                            CommonUtils.saveUserInfo(data);
                                        }
                                        String host = data.getString("public_ip");
                                        String key = data.getString("key");
                                        Config.getInstance().init(host,4900,key);
                                        Platform.runLater(()->{
                                            Index index = new Index(stage);
                                            Scene scene = new Scene(index.getContent(),250,320);
                                            stage.setScene(scene);
                                            stage.show();
                                        });
                                    }
                                }else {
                                    Platform.runLater(()->{
                                        JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
                                        JFXAlert alert = CommonUtils.alert(stage.getScene().getWindow(),
                                                new Label(CommonUtils.getLocaleContent("alertTitle")),
                                                new Label(CommonUtils.getLocaleContent("alertMsgUserNameOrPasswordError")),
                                                close);
                                        close.setOnAction(event1 -> alert.hideWithAnimation());
                                        alert.show();
                                    });
                                }
                            }else {
                                Platform.runLater(()->{
                                    JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
                                    JFXAlert alert = CommonUtils.alert(stage.getScene().getWindow(),
                                            new Label(CommonUtils.getLocaleContent("alertTitle")),
                                            new Label(object.getString("msg")),
                                            close);
                                    close.setOnAction(event1 -> alert.hideWithAnimation());
                                    alert.show();
                                });
                            }
                            System.out.println(s);
                        }catch (Exception exception){
                            Platform.runLater(()->{
                                JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
                                JFXAlert alert = CommonUtils.alert(stage.getScene().getWindow(),
                                        new Label(CommonUtils.getLocaleContent("alertTitle")),
                                        new Label(CommonUtils.getLocaleContent("alertMsgServiceError")),
                                        close);
                                close.setOnAction(event1 -> alert.hideWithAnimation());
                                alert.show();
                            });
                        }
                    }else {
                        Platform.runLater(()->{
                            JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
                            JFXAlert alert = CommonUtils.alert(stage.getScene().getWindow(),
                                    new Label(CommonUtils.getLocaleContent("alertTitle")),
                                    new Label(CommonUtils.getLocaleContent("alertMsgNetworkError")),
                                    close);
                            close.setOnAction(event1 -> alert.hideWithAnimation());
                            alert.show();
                        });
                    }
                }else {
                    Platform.runLater(()->{
                        JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
                        JFXAlert alert = CommonUtils.alert(stage.getScene().getWindow(),
                                new Label(CommonUtils.getLocaleContent("alertTitle")),
                                new Label(CommonUtils.getLocaleContent("alertMsgNetworkError")),
                                close);
                        close.setOnAction(event1 -> alert.hideWithAnimation());
                        alert.show();
                    });
                }
                Platform.runLater(()->{
                    loginButton.setDisable(false);
                    loginButton.setText(CommonUtils.getLocaleContent("login"));
                });
            }).start();

            OpenApi.getInstance().get("http://api.tbytm.com/open/v2/login?key=" + passWord, new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {

                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                }
            });
        });
    }

    public JFXButton getLoginButton() {
        return loginButton;
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }
}
