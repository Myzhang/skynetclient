package com.ld.view.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.ld.CommonUtils;
import com.ld.OpenApi;
import com.ld.netty.ClientFactory;
import com.ld.netty.common.Config;
import com.ld.view.Index;
import com.ld.view.Login;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ScheduledExecutorService;

@Slf4j
public class IndexController {

    private Index index;

    public IndexController(Index index){
        this.index = index;
        listening();
    }

    public void listening(){
        this.index.getCheckService().setOnAction(event->{
            JFXButton checkService = this.index.getCheckService();
            disableButton(checkService);
            checkService.setText(CommonUtils.getLocaleContent("alertLoading"));
            new Thread(()->{
                String msg = "";
                String s = OpenApi.getInstance().get("http://" + Config.getInstance().getHost() + ":20002/user/isOnline?key=" + Config.getInstance().getKey());
                if (s != null){
                    log.debug("/user/isOnline -> {}",s);
                    try {
                        JSONObject jsonObject = JSON.parseObject(s);
                        msg = jsonObject.getString("data");
                    }catch (Exception e) {
                        msg = s;
                    }
                    int status = ClientFactory.getInstance().getStatus();
                    if (status != 1 && ("在线".equals(msg) || "success".equals(msg))){
                        msg = CommonUtils.getLocaleContent("alertMsgClientError");
                    }
                }else {
                    msg = CommonUtils.getLocaleContent("alertMsgNetworkError");
                }
                final String alertMsg = msg;
                Platform.runLater(()->{
                    checkService.setDisable(false);
                    checkService.setText(CommonUtils.getLocaleContent("checkService"));
                    JFXButton closeButton = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
                    JFXAlert jfxAlert = CommonUtils.alert(
                            this.index.getContent().getScene().getWindow(),
                            new Label(CommonUtils.getLocaleContent("alertTitle")),
                            new Label(alertMsg),
                            closeButton);
                    closeButton.setOnAction(event2 -> jfxAlert.hideWithAnimation());
                    jfxAlert.show();
                });
            }).start();
        });
        this.index.getRestartService().setOnAction(event -> {
            disableButton(this.index.getRestartService());
            new Thread(()->{
                ClientFactory.getInstance().reStart();
                Platform.runLater(()->{
                    this.index.getRestartService().setDisable(false);
                    JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertClose"));
                    JFXAlert alert = CommonUtils.alert(this.index.getContent().getScene().getWindow(),
                            new Label(CommonUtils.getLocaleContent("alertTitle")),
                            new Label(CommonUtils.getLocaleContent("alertMsgRestartOk")),
                            close);
                    close.setOnAction(event2 -> alert.hideWithAnimation());
                    alert.show();
                });
            }).start();
        });
        this.index.getQuitLogin().setOnAction(event -> {
            JFXButton cancel = new JFXButton(CommonUtils.getLocaleContent("alertCancel"));
            JFXButton quit = new JFXButton(CommonUtils.getLocaleContent("alertQuitLogin"));
            JFXButton close = new JFXButton(CommonUtils.getLocaleContent("alertCloseService"));
            close.setTextFill(Paint.valueOf("#FF0000"));
            JFXAlert alert = CommonUtils.alert(this.index.getContent().getScene().getWindow(),
                    new Label(CommonUtils.getLocaleContent("alertTitle")),
                    new Label(CommonUtils.getLocaleContent("alertQuitLoginOrCloseService")),
                    close,quit,cancel);
            cancel.setOnAction(event2 -> alert.hideWithAnimation());
            quit.setOnAction(event2 -> {
                closedThread();
                quit.setText(CommonUtils.getLocaleContent("alertQuitLoading"));
                disableButton(quit,cancel,quit);
                new Thread(()->{
                    CommonUtils.delSystemTray();

                    ClientFactory.getInstance().stop();
                    Platform.runLater(()->{
                        alert.hideWithAnimation();
                        Stage stage = this.index.getStage();
                        stage.setScene(new Scene(new Login(stage).getAnchorPane(),250,320));
                        stage.show();
                    });
                }).start();
            });
            close.setOnAction(event2 -> {
                closedThread();
                close.setText(CommonUtils.getLocaleContent("alertCloseLoading"));
                disableButton(quit,cancel,quit);
                new Thread(()->{
                    ClientFactory.getInstance().stop();
                    Platform.runLater(()->{
                        alert.hideWithAnimation();
                        System.exit(0);
                    });
                }).start();
            });
            alert.show();
        });
    }

    public void closedThread(){
        ScheduledExecutorService statusExecutorService = this.index.getStatusExecutorService();
        statusExecutorService.shutdownNow();
    }

    public void disableButton(JFXButton... jfxButtons) {
        for (JFXButton jfxButton:jfxButtons){
            jfxButton.setDisable(true);
        }
    }
}
