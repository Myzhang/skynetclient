package com.ld.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRippler;
import com.ld.CommonUtils;
import com.ld.netty.ClientFactory;
import com.ld.netty.common.Config;
import com.ld.view.controller.IndexController;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import lombok.Getter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Index {

    @Getter
    private AnchorPane content;
    @Getter
    private JFXRippler title;
    @Getter
    private JFXRippler address;
    @Getter
    private JFXButton checkService;
    @Getter
    private JFXButton management;
    @Getter
    private JFXButton restartService;
    @Getter
    private JFXButton quitLogin;

    @Getter
    private Stage stage;
    @Getter
    ScheduledExecutorService statusExecutorService = Executors.newScheduledThreadPool(1);
    Label addressLabel;
    public Index(Stage stage){
        this.stage = stage;
        content = new AnchorPane();
        Label label = new Label(CommonUtils.getLocaleContent("appTitle"));
        label.setStyle("-fx-font-size: 32px");
        title = new JFXRippler(label);
        addressLabel = new Label(Config.getInstance().getHost());
        addressLabel.setStyle("-fx-font-size: 18px");
        addressLabel.setTextFill(Paint.valueOf("#000000"));
        address = new JFXRippler(addressLabel);
        address.setFocusTraversable(true);
        checkService = new JFXButton(CommonUtils.getLocaleContent("checkService"));
        checkService.setTextFill(Paint.valueOf("#FFFFFF"));
        checkService.setStyle("-fx-background-color: #000000;-fx-font-size: 14px");

        management = new JFXButton(CommonUtils.getLocaleContent("management"));
        management.setTextFill(Paint.valueOf("#FFFFFF"));
        management.setStyle("-fx-background-color: #000000;-fx-font-size: 14px");

        restartService = new JFXButton(CommonUtils.getLocaleContent("restartService"));
        restartService.setTextFill(Paint.valueOf("#FFFFFF"));
        restartService.setStyle("-fx-background-color: #000000;-fx-font-size: 14px");

        quitLogin = new JFXButton(CommonUtils.getLocaleContent("quitLogin"));
        quitLogin.setTextFill(Paint.valueOf("#FFFFFF"));
        quitLogin.setStyle("-fx-background-color: #000000;-fx-font-size: 14px");
        content.getChildren().addAll(title,address,checkService,management,restartService,quitLogin);
        setLocation();
        new IndexController(this);
        agentConnection();
    }

    public void agentConnection(){
        ClientFactory.getInstance().start();
        CommonUtils.systemTray();
        statusExecutorService.scheduleAtFixedRate(()->{
            int status = ClientFactory.getInstance().getStatus();
            if (status == 0){//离线
                Platform.runLater(()->{
                    CommonUtils.setTrayIcon(status);
                    addressLabel.setTextFill(Paint.valueOf("#FF0000"));
                });
            }else {//在线
                Platform.runLater(()->{
                    CommonUtils.setTrayIcon(status);
                    addressLabel.setTextFill(Paint.valueOf("#008000"));
                });
            }
        },1,1, TimeUnit.SECONDS);
    }

    public void setLocation(){
        AnchorPane.setTopAnchor(title,30.0);
        AnchorPane.setLeftAnchor(title,30.0);
        AnchorPane.setRightAnchor(title,30.0);

        AnchorPane.setTopAnchor(address,100.0);
        AnchorPane.setLeftAnchor(address,30.0);
        AnchorPane.setRightAnchor(address,30.0);

        AnchorPane.setBottomAnchor(checkService,150.0);
        AnchorPane.setLeftAnchor(checkService,30.0);
        AnchorPane.setRightAnchor(checkService,30.0);

        AnchorPane.setBottomAnchor(management,110.0);
        AnchorPane.setLeftAnchor(management,30.0);
        AnchorPane.setRightAnchor(management,30.0);

        AnchorPane.setBottomAnchor(restartService,70.0);
        AnchorPane.setLeftAnchor(restartService,30.0);
        AnchorPane.setRightAnchor(restartService,30.0);

        AnchorPane.setBottomAnchor(quitLogin,30.0);
        AnchorPane.setLeftAnchor(quitLogin,30.0);
        AnchorPane.setRightAnchor(quitLogin,30.0);
    }

    public AnchorPane getContent() {
        return content;
    }
}
