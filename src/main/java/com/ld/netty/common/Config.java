package com.ld.netty.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 读取配置文件 默认的config.properties 和自定义都支持
 *
 */
public class Config {

    private static Map<String, Config> instances = new ConcurrentHashMap<String, Config>();

    static Config config = new Config();
    String host;
    Integer port;
    String key;

    public static Config getInstance() {
        return config;
    }


    public void init(String host,Integer port,String key){
        this.host = host;
        this.port = port;
        this.key = key;
    }

    public String getHost(){
        return host;
    }

    public Integer getPort(){
        return port;
    }

    public String getKey(){
        return key;
    }
}