package com.ld.netty.common.container;

public interface Container {

    void start();

    void stop();
}
