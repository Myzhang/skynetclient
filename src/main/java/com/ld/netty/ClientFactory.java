package com.ld.netty;

import com.ld.CommonUtils;
import com.ld.netty.common.Config;

public class ClientFactory {
    static ClientFactory clientFactory = new ClientFactory();
    ProxyClientContainer proxyClientContainer;
    private int status;

    public ClientFactory(){

    }
    public static ClientFactory getInstance(){
        return clientFactory;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status){
        this.status = status;
    }

    public void start() {
        if (proxyClientContainer == null){
            CommonUtils.setSysLogin(Config.getInstance().getKey());
            proxyClientContainer = new ProxyClientContainer();
            new Thread(()->{
                proxyClientContainer.start();
            }).start();
        }
    }

    public boolean isStart(){
        if (proxyClientContainer != null){
            return true;
        }
        return false;
    }

    public void reStart(){
        if (proxyClientContainer != null){
            CommonUtils.delSysLogin(Config.getInstance().getKey());
            proxyClientContainer.quit();
            proxyClientContainer = null;
            start();
        }
    }

    public void stop() {
        CommonUtils.delSysLogin(Config.getInstance().getKey());
        if (proxyClientContainer != null){
            proxyClientContainer.quit();
            proxyClientContainer = null;
        }
    }
}
