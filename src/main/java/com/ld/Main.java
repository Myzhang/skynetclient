package com.ld;

import com.jfoenix.controls.JFXButton;
import com.ld.netty.ClientFactory;
import com.ld.netty.common.Config;
import com.ld.view.Login;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

    static {
        CommonUtils.initIons();
        CommonUtils.initSkynet();
    }

    @Override
    public void init() {
        Platform.setImplicitExit(false);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> CommonUtils.delSysLogin(Config.getInstance().getKey())));
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle(CommonUtils.getLocaleContent("appTitle"));
        ObservableList<Image> icons = primaryStage.getIcons();
        icons.add(CommonUtils.getSkynetTitle());
        CommonUtils.setStage(primaryStage);
        Login login = new Login(primaryStage);
        AnchorPane anchorPane = login.getAnchorPane();
        Scene scene = new Scene(anchorPane,250,320);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
        if (CommonUtils.isAutoLogin()){
            JFXButton loginButton = login.getLoginButton();
            loginButton.fire();
        }
        primaryStage.setOnCloseRequest(event ->{
            if (!ClientFactory.getInstance().isStart()){
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
